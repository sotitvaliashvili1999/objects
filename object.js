// Subject credit object
const credit = {
  js: 4,
  react: 7,
  python: 6,
  java: 3,
};
credit.sumOfCredits = credit.js + credit.react + credit.python + credit.java;

// students main array and four objects
const student = [];

student[0] = {
  firstName: "Jean",
  lastName: "Reno",
  age: 26,
  subject: {
    frontEnd: {
      javascript: 62,
      react: 57,
    },
    backEnd: {
      python: 88,
      java: 90,
    },
  },
  statistic: {},
};
student[0].statistic.sumOfScores =
  student[0].subject.frontEnd.javascript +
  student[0].subject.frontEnd.react +
  student[0].subject.backEnd.python +
  student[0].subject.backEnd.java;
student[0].statistic.average = student[0].statistic.sumOfScores / 4;
student[0].statistic.gpa =
  (credit.js + 0.5 * credit.react + 3 * credit.python + 3 * credit.java) /
  credit.sumOfCredits;
// -----------------------------------------//
student[1] = {
  firstName: "Klod",
  lastName: "Mone",
  age: 19,
  subject: {
    frontEnd: {
      javascript: 77,
      react: 52,
    },
    backEnd: {
      python: 92,
      java: 67,
    },
  },
  statistic: {},
};
student[1].statistic.sumOfScores =
  student[1].subject.frontEnd.javascript +
  student[1].subject.frontEnd.react +
  student[1].subject.backEnd.python +
  student[1].subject.backEnd.java;
student[1].statistic.average = student[1].statistic.sumOfScores / 4;
student[1].statistic.gpa =
  (2 * credit.js + 0.5 * credit.react + 4 * credit.python + credit.java) /
  credit.sumOfCredits;
// --------------------------------------//
student[2] = {
  firstName: "Van",
  lastName: "Gogh",
  age: 21,
  subject: {
    frontEnd: {
      javascript: 51,
      react: 98,
    },
    backEnd: {
      python: 65,
      java: 70,
    },
  },
  statistic: {},
};
student[2].statistic.sumOfScores =
  student[2].subject.frontEnd.javascript +
  student[2].subject.frontEnd.react +
  student[2].subject.backEnd.python +
  student[2].subject.backEnd.java;
student[2].statistic.average = student[2].statistic.sumOfScores / 4;
student[2].statistic.gpa =
  (0.5 * credit.js + 4 * credit.react + credit.python + credit.java) /
  credit.sumOfCredits;
//------------------------------------------//
student[3] = {
  firstName: "Dam",
  lastName: "Square",
  age: 36,
  subject: {
    frontEnd: {
      javascript: 82,
      react: 53,
    },
    backEnd: {
      python: 80,
      java: 65,
    },
  },
  statistic: {},
};
student[3].statistic.sumOfScores =
  student[3].subject.frontEnd.javascript +
  student[3].subject.frontEnd.react +
  student[3].subject.backEnd.python +
  student[3].subject.backEnd.java;
student[3].statistic.average = student[3].statistic.sumOfScores / 4;
student[3].statistic.gpa =
  (3 * credit.js + 0.5 * credit.react + 2 * credit.python + credit.java) /
  credit.sumOfCredits;

// average mark of all students
const avgMarkOfAllStudent =
  (student[0].statistic.sumOfScores +
    student[1].statistic.sumOfScores +
    student[2].statistic.sumOfScores +
    student[3].statistic.sumOfScores) /
  16;
// student degree
student[0].degree =
  student[0].statistic.average > avgMarkOfAllStudent
    ? "Red Diploma"
    : "Enemy of people";
student[1].degree =
  student[1].statistic.average > avgMarkOfAllStudent
    ? "Red Diploma"
    : "Enemy of people";
student[2].degree =
  student[2].statistic.average > avgMarkOfAllStudent
    ? "Red Diploma"
    : "Enemy of people";
student[3].degree =
  student[3].statistic.average > avgMarkOfAllStudent
    ? "Red Diploma"
    : "Enemy of people";

// Highest GPA

let bestStudentWithGpa = undefined;
if (
  student[0].statistic.gpa > student[1].statistic.gpa &&
  student[0].statistic.gpa > student[2].statistic.gpa &&
  student[0].statistic.gpa > student[3].statistic.gpa
) {
  bestStudentWithGpa = student[0];
} else if (
  student[1].statistic.gpa > student[2].statistic.gpa &&
  student[1].statistic.gpa > student[3].statistic.gpa
) {
  bestStudentWithGpa = student[1];
} else if (student[2].statistic.gpa > student[3].statistic.gpa) {
  bestStudentWithGpa = student[2];
} else {
  bestStudentWithGpa = student[3];
}

console.log(
  `The highest GPA --> ${bestStudentWithGpa.statistic.gpa} has ${bestStudentWithGpa.firstName} ${bestStudentWithGpa.lastName};`
);

// The best  student over 21 with average mark
let bestStudentWithAvg = undefined;
if (
  student[0].age > 21 &&
  student[0].statistic.average > student[1].statistic.average &&
  student[0].statistic.average > student[2].statistic.average &&
  student[0].statistic.average > student[3].statistic.average
) {
  bestStudentWithAvg = student[0];
} else if (
  student[1].age > 21 &&
  student[1].statistic.average > student[2].statistic.average &&
  student[1].statistic.average > student[3].statistic.average
) {
  bestStudentWithAvg = student[1];
} else if (
  student[2].age > 21 &&
  student[2].statistic.average > student[3].statistic.average
) {
  bestStudentWithAvg = student[2];
} else if (student[3].age > 21) {
  bestStudentWithAvg = student[3];
} else {
  console.log("No one is over 21");
}
console.log(
  `The highest Average mark --> ${bestStudentWithAvg.statistic.average} has ${bestStudentWithAvg.firstName} ${bestStudentWithAvg.lastName};`
);

// The bets in Front-end subjects;
const frontEndStudent1 =
  (student[0].subject.frontEnd.javascript + student[0].subject.frontEnd.react) /
  2;
const frontEndStudent2 =
  (student[1].subject.frontEnd.javascript + student[1].subject.frontEnd.react) /
  2;
const frontEndStudent3 =
  (student[2].subject.frontEnd.javascript + student[2].subject.frontEnd.react) /
  2;
const frontEndStudent4 =
  (student[3].subject.frontEnd.javascript + student[3].subject.frontEnd.react) /
  2;

let bestInFrontEnd = undefined;

if (
  frontEndStudent1 > frontEndStudent2 &&
  frontEndStudent1 > frontEndStudent3 &&
  frontEndStudent1 > frontEndStudent4
) {
  bestInFrontEnd = student[0];
} else if (
  frontEndStudent2 > frontEndStudent3 &&
  frontEndStudent2 > frontEndStudent4
) {
  bestInFrontEnd = student[1];
} else if (frontEndStudent3 > frontEndStudent4) {
  bestInFrontEnd = student[2];
} else {
  bestInFrontEnd = student[3];
}
console.log(
  `The best student is ${bestInFrontEnd.firstName} ${bestInFrontEnd.lastName} in Front-end subjects;`
);

